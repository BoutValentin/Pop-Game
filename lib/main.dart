import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:popgame/models/Board.dart';
import 'package:popgame/models/SimpleTile.dart';
import 'package:popgame/models/Tile.dart';
import 'package:popgame/providers/game_providers.dart';
import 'package:flutter_svg/flutter_svg.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pop Game',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.red,
      ),
      home: const Home(),
    );
  }
}

class Home extends ConsumerStatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _HomeState();
}

class _HomeState extends ConsumerState<Home> {
  @override
  void initState() {}

  @override
  Widget build(BuildContext context) {
    final game = ref.watch(gameProvider);
    final gameRef = ref.read(gameProvider.notifier);
    double width = MediaQuery.of(context).size.width;
    if (width > 516) width = 516;
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.elliptical(19, 8),
                bottomRight: Radius.elliptical(19, 8))),
        centerTitle: true,
        title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text("Pop Game"),
              const SizedBox(
                width: 7,
              ),
              Image.asset(
                'assets/images/pop.png',
                height: 30,
              )
            ]),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      game.isFinish
                          ? "${game.activePlayer.name} HAS WIN !!"
                          : "It's up to ${game.activePlayer.name}",
                      style: const TextStyle(
                          fontWeight: FontWeight.w600, fontSize: 18),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Container(
                      constraints: BoxConstraints(
                          maxWidth: width - 16, maxHeight: width - 16),
                      child: Stack(
                        children: [
                          Positioned(
                              top: 0,
                              left: 0,
                              child: Container(
                                  child: BoardWidget(
                                      board: game.board, showHotZone: false))),
                          if (game.showHotZone)
                            Positioned(
                                top: 0,
                                left: 0,
                                child: Opacity(
                                    opacity: 0.55,
                                    child: BoardWidget(
                                        board: game.board, showHotZone: true)))
                        ],
                      ),
                    ),
                    if (game.board.perCentTileDiscover > 50)
                      const SizedBox(
                        height: 20,
                      ),
                    if (game.board.perCentTileDiscover > 50)
                      TextButton(
                          style: ButtonStyle(
                              elevation: MaterialStateProperty.resolveWith(
                                  (states) => 3),
                              foregroundColor:
                                  MaterialStateProperty.resolveWith(
                                      (states) => Colors.white),
                              backgroundColor:
                                  MaterialStateProperty.resolveWith(
                                      (states) => Colors.red.shade900)),
                          onPressed: () {
                            if (game.showHotZone) {
                              gameRef.hideHotZone();
                              return;
                            }
                            gameRef.showHotZone();
                          },
                          child: Text(
                              "Help: ${game.showHotZone ? "Hide" : "Show"} Hot zone")),
                    const SizedBox(
                      height: 20,
                    ),
                    TextButton(
                        style: ButtonStyle(
                            elevation: MaterialStateProperty.resolveWith(
                                (states) => 1),
                            backgroundColor: MaterialStateProperty.resolveWith(
                                (states) => Colors.white)),
                        onPressed: () {
                          ref.read(gameProvider.notifier).resetGame();
                        },
                        child: const Text("Replay !")),
                    const SizedBox(
                      height: 20,
                    ),
                    Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10,
                      runSpacing: 8,
                      children: [
                        TextButton(
                            style: ButtonStyle(
                                elevation: MaterialStateProperty.resolveWith(
                                    (states) => 1),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (states) => Colors.white)),
                            onPressed: () {
                              ref.read(gameProvider.notifier).createEasyGame();
                            },
                            child: const Text("Create Easy Game")),
                        TextButton(
                            style: ButtonStyle(
                                elevation: MaterialStateProperty.resolveWith(
                                    (states) => 1),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (states) => Colors.white)),
                            onPressed: () {
                              ref
                                  .read(gameProvider.notifier)
                                  .createRegularGame();
                            },
                            child: const Text("Create Normal Game")),
                        TextButton(
                            style: ButtonStyle(
                                elevation: MaterialStateProperty.resolveWith(
                                    (states) => 1),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith(
                                        (states) => Colors.white)),
                            onPressed: () {
                              ref.read(gameProvider.notifier).createHardGame();
                            },
                            child: const Text("Create Hard Game")),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                        constraints: const BoxConstraints(maxWidth: 500),
                        child: const FormCustomGame()),
                    const SizedBox(
                      height: 30,
                    ),
                    const Text(
                        "Made with ❤️ by @boutvalentin when you are bored"),
                    const SizedBox(
                      height: 30,
                    ),
                  ]),
            ),
          ),
        ),
      ),
    );
  }
}

class BoardWidget extends ConsumerWidget {
  final Board board;
  final bool showHotZone;
  const BoardWidget({Key? key, required this.board, required this.showHotZone})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    double width = MediaQuery.of(context).size.width;
    if (width > 516) width = 516;
    return Container(
      constraints: BoxConstraints(
        maxWidth: width - 16,
        maxHeight: width - 16,
      ),
      child: Column(
        children: board.board
            .asMap()
            .map((i, e) => MapEntry(
                i,
                Expanded(
                  flex: board.size,
                  child: Row(
                    children: e
                        .asMap()
                        .map((j, f) => MapEntry(
                            j,
                            Expanded(
                                flex: board.size,
                                child: TileWidget(
                                  tile: f,
                                  x: i,
                                  y: j,
                                  showHot: showHotZone,
                                ))))
                        .values
                        .toList(),
                  ),
                )))
            .values
            .toList(),
      ),
    );
  }
}

class TileWidget extends ConsumerWidget {
  final Tile tile;
  final bool showHot;
  final int x;
  final int y;
  const TileWidget(
      {Key? key,
      required this.tile,
      required this.x,
      required this.y,
      this.showHot = false})
      : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return GestureDetector(
      onTap: () {
        final notifi = ref.read(gameProvider.notifier);
        notifi.hideHotZone();
        if (!notifi.isFinish) {
          final res = notifi.touchTile(x, y);
          if (res) {
            if (notifi.isFinish) {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                    content: Text("${notifi.state.activePlayer.name} WIN!")),
              );
            } else {
              notifi.changeTurn();
            }
          }
        }
      },
      child: Padding(
        padding: const EdgeInsets.all(2.0),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: hotDiscoverOrNotWidget()),
      ),
    );
  }

  Widget hotDiscoverOrNotWidget() {
    return showHot ? hotWidget() : discoverOrNotWidget();
  }

  Widget hotWidget() {
    return tile.isHot
        ? Container(child: SvgPicture.asset('assets/images/hot.svg'))
        : discoverOrNotWidget();
  }

  Widget discoverOrNotWidget() {
    return tile.isDicover ? discoverWidget() : undiscoverWidget();
  }

  Widget undiscoverWidget() {
    return Material(
      elevation: 1,
      child: Container(
        decoration: BoxDecoration(color: Colors.red.shade800),
        child: Opacity(
          opacity: 1,
          child: Image.asset('assets/images/pop.png'),
        ),
      ),
    );
  }

  Widget discoverWidget() {
    return tile is SimpleTile ? simpleTile() : SvgPicture.asset(tile.asset);
  }

  Widget simpleTile() {
    return Material(
      elevation: 1,
      child: Container(
        decoration: BoxDecoration(color: Colors.grey.shade300),
      ),
    );
  }
}

class FormCustomGame extends ConsumerStatefulWidget {
  const FormCustomGame({
    Key? key,
  }) : super(key: key);

  @override
  ConsumerState<ConsumerStatefulWidget> createState() => _FormCustomGameState();
}

class _FormCustomGameState extends ConsumerState<FormCustomGame> {
  final _formkey = GlobalKey<FormState>();
  int? size = 8;
  int? malus = 2;
  int? bonus = 2;
  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formkey,
        child: Column(
          children: [
            TextFormField(
              keyboardType: TextInputType.number,
              initialValue: "$size",
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(label: Text("Size*")),
              onChanged: (String? s) {
                if (s != null) {
                  size = int.parse(s);
                }
              },
              validator: (String? s) {
                if (s == null || s.isEmpty) return "Size cannot be empty";
                if (int.parse(s) > 18) {
                  return "Please provide a size less than 18";
                }
              },
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              initialValue: "$bonus",
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(label: Text("Bonus*")),
              onChanged: (String? s) {
                if (s != null) {
                  bonus = int.parse(s);
                }
              },
              validator: (String? s) {
                if (s == null || s.isEmpty) return "Bonus cannot be empty";
                if (size == null) {
                  return "Size should be define to calculate bonys";
                }
                if (int.parse(s) > (size! * size!)) {
                  return "You should provide a number of bonus less than the size of the map";
                }
              },
            ),
            TextFormField(
              keyboardType: TextInputType.number,
              initialValue: "$malus",
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              decoration: const InputDecoration(label: Text("Malus*")),
              onChanged: (String? s) {
                if (s != null) {
                  malus = int.parse(s);
                }
              },
              validator: (String? s) {
                if (s == null || s.isEmpty) return "Malus cannot be empty";
                if (size == null) {
                  return "Size should be define to calculate malus";
                }
                if (int.parse(s) > (size! * size!)) {
                  return "You should provide a number of malus less than the size of the map";
                }
              },
            ),
            const SizedBox(
              height: 10,
            ),
            TextButton(
                style: ButtonStyle(
                    elevation: MaterialStateProperty.resolveWith((states) => 1),
                    backgroundColor: MaterialStateProperty.resolveWith(
                        (states) => Colors.white)),
                onPressed: () {
                  if (malus == null || bonus == null || size == null) return;
                  if (_formkey.currentState!.validate() &&
                      (malus! + bonus!) < (size! * size!)) {
                    ref
                        .read(gameProvider.notifier)
                        .createCustomGame(size!, malus!, bonus!);
                  }
                },
                child: const Text("Create Custom Game"))
          ],
        ));
  }
}
