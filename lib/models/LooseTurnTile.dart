import 'package:popgame/models/WinLooseTurnTile.dart';
import 'dart:core';

class LooseTurnTile extends WinLooseTurnTile {
  LooseTurnTile({required int winloose})
      : super(
            winloose: winloose,
            isDicover: false,
            asset: "assets/images/looseturn-${winloose.abs()}.svg",
            text: "Loose $winloose turn",
            isHot: false);
}
