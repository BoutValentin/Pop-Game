import 'package:popgame/models/Player.dart';
import 'package:popgame/models/Tile.dart';

class WinTile extends Tile {
  static const type = 'win';
  WinTile({required bool isDicover})
      : super(
            isDicover: isDicover,
            asset: 'assets/images/gold.svg',
            text: "dab",
            isHot: false);

  WinTile.notDiscover()
      : super.notDiscover(asset: 'assets/images/gold.svg', text: "dab");
  @override
  void makeAction(Player player) {
    player.win();
  }

  Map<String, dynamic> toMap() {
    return {
      'type': WinTile.type,
      'isDiscover': isDicover,
      'asset': asset,
      'text': text,
      'isHot': isHot,
    };
  }
}
