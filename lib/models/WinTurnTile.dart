import 'package:popgame/models/WinLooseTurnTile.dart';

class WinTurnTile extends WinLooseTurnTile {
  WinTurnTile({
    required int winloose,
  }) : super(
            winloose: winloose,
            isDicover: false,
            asset: "assets/images/winturn-${winloose.abs()}.svg",
            text: "Win $winloose turn",
            isHot: false);
}
