import 'dart:convert';

class Player {
  int numberTurn;
  String name;
  bool asWin;
  Player({
    required this.numberTurn,
    required this.name,
    required this.asWin,
  });

  Player.simple({required this.name})
      : numberTurn = 0,
        asWin = false;

  bool get canPlay => numberTurn >= 0;

  bool get canReplay => numberTurn > 0;

  Player copyWith({
    int? numberTurn,
    String? name,
    bool? asWin,
  }) {
    return Player(
      numberTurn: numberTurn ?? this.numberTurn,
      name: name ?? this.name,
      asWin: asWin ?? this.asWin,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'numberTurn': numberTurn,
      'name': name,
      'asWin': asWin,
    };
  }

  factory Player.fromMap(Map<String, dynamic> map) {
    return Player(
      numberTurn: map['numberTurn']?.toInt() ?? 0,
      name: map['name'] ?? '',
      asWin: map['asWin'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory Player.fromJson(String source) => Player.fromMap(json.decode(source));

  @override
  String toString() =>
      'Player(numberTurn: $numberTurn, name: $name, asWin: $asWin)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Player &&
        other.numberTurn == numberTurn &&
        other.name == name &&
        other.asWin == asWin;
  }

  @override
  int get hashCode => numberTurn.hashCode ^ name.hashCode ^ asWin.hashCode;

  void addOrRemoveTurn(int turn) {
    numberTurn += turn;
  }

  void passturn() {
    if (numberTurn < 0) numberTurn++;
  }

  void asReplay() {
    if (numberTurn > 0) numberTurn--;
  }

  void win() {
    asWin = true;
  }
}
