import 'package:popgame/models/Player.dart';
import 'package:popgame/models/SimpleTile.dart';
import 'package:popgame/models/WinTile.dart';

abstract class Tile {
  static const type = 'default';
  bool isDicover;
  bool isHot;
  String asset;
  String text;

  Tile({
    required this.isDicover,
    required this.asset,
    required this.text,
    required this.isHot,
  });

  Tile.notDiscover({
    required this.asset,
    required this.text,
  })  : isDicover = false,
        isHot = false;

  @override
  String toString() =>
      'Tile(isDicover: $isDicover, asset: $asset, text: $text)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Tile &&
        other.isDicover == isDicover &&
        other.asset == asset &&
        other.isHot == isHot &&
        other.text == text;
  }

  @override
  int get hashCode =>
      isDicover.hashCode ^ asset.hashCode ^ text.hashCode ^ isHot.hashCode;

  void discover() {
    isDicover = true;
  }

  void unDiscover() {
    isDicover = false;
  }

  void makeAction(Player player);

  Map<String, dynamic> toMap() {
    return {
      'isDiscover': isDicover,
      'isHot': isHot,
      'asset': asset,
      'text': text,
    };
  }

  factory Tile.fromMap(Map<String, dynamic> map) {
    if (map['type'] == WinTile.type) {
      return WinTile(isDicover: map['isDiscover'] ?? false);
    }
    return SimpleTile(isDicover: map['isDiscover'] ?? false);
  }
}
