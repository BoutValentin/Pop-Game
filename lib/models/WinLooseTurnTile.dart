import 'package:popgame/models/Player.dart';
import 'package:popgame/models/Tile.dart';

class WinLooseTurnTile extends Tile {
  final int winloose;

  WinLooseTurnTile(
      {required this.winloose,
      required bool isDicover,
      required String asset,
      required String text,
      required bool isHot})
      : super(isDicover: isDicover, asset: asset, text: text, isHot: isHot);

  @override
  void makeAction(Player player) {
    print("${player.name} hit a winloose, it should win/loose ${winloose}");
    player.addOrRemoveTurn(winloose);
  }
}
