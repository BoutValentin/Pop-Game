import 'dart:convert';
import 'dart:math';

import 'package:popgame/models/LooseTurnTile.dart';
import 'package:popgame/models/SimpleTile.dart';
import 'package:popgame/models/Tile.dart';
import 'package:popgame/models/WinTile.dart';
import 'package:popgame/models/WinTurnTile.dart';

class Generator {
  int size;
  int bonus;
  int malus;

  Generator({
    required this.size,
    required this.bonus,
    required this.malus,
  });

  Generator copyWith({
    int? size,
    int? bonus,
    int? malus,
  }) {
    return Generator(
      size: size ?? this.size,
      bonus: bonus ?? this.bonus,
      malus: malus ?? this.malus,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'size': size,
      'bonus': bonus,
      'malus': malus,
    };
  }

  factory Generator.fromMap(Map<String, dynamic> map) {
    return Generator(
      size: map['size']?.toInt() ?? 0,
      bonus: map['bonus']?.toInt() ?? 0,
      malus: map['malus']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Generator.fromJson(String source) =>
      Generator.fromMap(json.decode(source));

  @override
  String toString() => 'Generator(size: $size, bonus: $bonus, malus: $malus)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Generator &&
        other.size == size &&
        other.bonus == bonus &&
        other.malus == malus;
  }

  @override
  int get hashCode => size.hashCode ^ bonus.hashCode ^ malus.hashCode;

  List<List<Tile>> generateBoard() {
    List<List<Tile>> board = List.generate(
        size, (_) => List.generate(size, (_) => SimpleTile.notDiscover()));
    Random rnd = Random();
    int winX = rnd.nextInt(size);
    int winY = rnd.nextInt(size);
    board[winX][winY] = WinTile.notDiscover();
    makeHotZoneAround(winX, winY, board);
    createBonus('bonus', board);
    createBonus('malus', board);
    return board;
  }

  void createBonus(String wanted, List<List<Tile>> board) {
    Random rnd = Random();
    int toCreate = wanted == 'bonus' ? bonus : malus;
    int i = 0;
    for (i = 0; i < toCreate; i++) {
      int x = rnd.nextInt(size);
      int y = rnd.nextInt(size);
      if (board[x][y] is! SimpleTile) {
        i--;
      } else {
        int bonusMalus = i < (toCreate / 2) ? 1 : 3;
        board[x][y] = wanted == 'bonus'
            ? WinTurnTile(winloose: bonusMalus)
            : LooseTurnTile(winloose: -bonusMalus);
        makeHotZoneAround(x, y, board);
      }
    }
  }

  void makeHotZoneAround(int x, int y, List<List<Tile>> board) {
    final rnd = Random();
    for (int i = x - 1; i < x + 2; i++) {
      if (i < size && i >= 0) {
        for (int j = y - 1; j < y + 2; j++) {
          if (j < size && j >= 0) {
            board[i][j].isHot = rnd.nextBool();
          }
        }
      }
    }
  }
}
