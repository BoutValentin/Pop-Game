import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:popgame/models/Board.dart';
import 'package:popgame/models/Generator.dart';

import 'package:popgame/models/Player.dart';
import 'package:popgame/models/Tile.dart';

class Game {
  Board board;
  List<Player> players;
  int playerTurn;
  bool showHotZone;
  Game({
    required int size,
    required this.players,
    required this.playerTurn,
  })  : board = Board.generate(size: size),
        showHotZone = false;

  Game.withBoard(
      {required this.board,
      required this.players,
      required this.playerTurn,
      required this.showHotZone});

  Game copyWith(
      {Board? board,
      List<Player>? players,
      int? playerTurn,
      bool? showHotZone}) {
    return Game.withBoard(
        board: board ?? this.board,
        players: players ?? this.players,
        playerTurn: playerTurn ?? this.playerTurn,
        showHotZone: showHotZone ?? this.showHotZone);
  }

  Map<String, dynamic> toMap() {
    return {
      'board': board.toMap(),
      'players': players.map((x) => x.toMap()).toList(),
      'playerTurn': playerTurn,
    };
  }

  factory Game.fromMap(Map<String, dynamic> map) {
    return Game.withBoard(
        board: Board.fromMap(map['board']),
        players:
            List<Player>.from(map['players']?.map((x) => Player.fromMap(x))),
        playerTurn: map['playerTurn']?.toInt() ?? 0,
        showHotZone: map['showHotZone'] ?? false);
  }

  String toJson() => json.encode(toMap());

  factory Game.fromJson(String source) => Game.fromMap(json.decode(source));

  @override
  String toString() =>
      'Game(board: $board, players: $players, playerTurn: $playerTurn)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Game &&
        other.board == board &&
        listEquals(other.players, players) &&
        other.playerTurn == playerTurn;
  }

  @override
  int get hashCode => board.hashCode ^ players.hashCode ^ playerTurn.hashCode;

  bool touchTile(Player p, int x, int y) {
    return board.touchTile(p, x, y);
  }

  Player get activePlayer => players[playerTurn % (players.length)];

  bool get isFinish =>
      players.map((e) => e.asWin).reduce((value, element) => value || element);
}
