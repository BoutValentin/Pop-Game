import 'package:popgame/models/Player.dart';
import 'package:popgame/models/Tile.dart';

class SimpleTile extends Tile {
  static const type = 'simple';
  SimpleTile({required bool isDicover})
      : super(
            isDicover: isDicover,
            asset: 'assets/images/pop.svg',
            text: "pop",
            isHot: false);

  SimpleTile.notDiscover()
      : super.notDiscover(asset: 'assets/images/pop.svg', text: "pop");

  @override
  void makeAction(Player player) {}

  Map<String, dynamic> toMap() {
    return {
      'type': SimpleTile.type,
      'isDiscover': isDicover,
      'asset': asset,
      'text': text,
      'isHot': isHot,
    };
  }
}
