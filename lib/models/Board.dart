import 'package:popgame/exceptions/out_board_exceptions.dart';
import 'package:popgame/models/Generator.dart';
import 'package:popgame/models/Player.dart';
import 'package:popgame/models/Tile.dart';

class Board {
  List<List<Tile>> board;
  int size;

  Board({required this.size, required this.board});

  Board.generate({required this.size, bonus = 2, malus = 3})
      : board =
            Generator(size: size, bonus: bonus, malus: malus).generateBoard();

  bool touchTile(Player p, int x, int y) {
    if (x >= size || y >= size) {
      throw OutBoardExceptions();
    }
    Tile t = board[x][y];
    if (t.isDicover) {
      return false;
    }
    t.discover();
    t.makeAction(p);
    return true;
  }

  Map<String, dynamic> toMap() {
    return {
      'board': board.map((x) => x.map((e) => e.toMap()).toList()).toList(),
      'size': size
    };
  }

  factory Board.fromMap(Map<String, dynamic> map) {
    return Board(
        size: map['size'] ?? 0,
        board: List<List<Tile>>.from(map['board']
            ?.map((x) => List<Tile>.from(x.map((e) => Tile.fromMap(map))))));
  }

  @override
  String toString() {
    String s = "\n";
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        s += "| ${board[i][j].isDicover} |";
      }
      s += '\n';
    }
    s += '\n\n';
    return s;
  }

  int get perCentTileDiscover {
    int counter = 0;
    for (final li in board) {
      for (final tile in li) {
        if (tile.isDicover) counter++;
      }
    }

    return ((counter * 100) / (size * size)).round();
  }
}
