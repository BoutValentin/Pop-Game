import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:popgame/exceptions/out_board_exceptions.dart';
import 'package:popgame/models/Board.dart';
import 'package:popgame/models/Game.dart';
import 'package:popgame/models/Player.dart';

class GameNotifier extends StateNotifier<Game> {
  GameNotifier()
      : super(Game(
            size: 8,
            players: [
              Player.simple(name: "Player 1"),
              Player.simple(name: "Player 2")
            ],
            playerTurn: 0));

  void changeTurn() {
    Player current = state.players[state.playerTurn % (state.players.length)];
    if (current.canReplay && current.canPlay) {
      current.asReplay();
      state = state.copyWith();
      return;
    }
    int pp = ++state.playerTurn % (state.players.length);
    Player next = state.players[pp];
    if (!next.canPlay) {
      next.passturn();
      state = state.copyWith();
      changeTurn();
    }
  }

  bool touchTile(int x, int y) {
    bool res = true;
    try {
      res = state.touchTile(
          state.players[state.playerTurn % (state.players.length)], x, y);

      state = state.copyWith();
    } on OutBoardExceptions {
      return false;
    }
    return res;
  }

  void showHotZone() {
    state = state.copyWith(showHotZone: true);
  }

  void hideHotZone() {
    state = state.copyWith(showHotZone: false);
  }

  void resetGame() {
    state = Game(
        size: state.board.size,
        players: [
          Player.simple(name: "Player 1"),
          Player.simple(name: "Player 2")
        ],
        playerTurn: 0);
  }

  void createEasyGame() {
    state = Game(
        size: 5,
        players: [
          Player.simple(name: "Player 1"),
          Player.simple(name: "Player 2")
        ],
        playerTurn: 0);
  }

  void createRegularGame() {
    state = Game(
        size: 8,
        players: [
          Player.simple(name: "Player 1"),
          Player.simple(name: "Player 2")
        ],
        playerTurn: 0);
  }

  void createHardGame() {
    state = Game(
        size: 15,
        players: [
          Player.simple(name: "Player 1"),
          Player.simple(name: "Player 2")
        ],
        playerTurn: 0);
  }

  void createCustomGame(int size, int malus, int bonus) {
    state = Game.withBoard(
        board: Board.generate(size: size, bonus: bonus, malus: malus),
        players: [
          Player.simple(name: "Player 1"),
          Player.simple(name: "Player 2")
        ],
        playerTurn: 0,
        showHotZone: false);
  }

  bool get isFinish => state.isFinish;
}

final gameProvider = StateNotifierProvider<GameNotifier, Game>((ref) {
  return GameNotifier();
});
